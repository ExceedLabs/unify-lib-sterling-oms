class XMLMock {
  
  static checkProductId(productId) {

    if(productId === "10-brigadeiros-box-tower" || productId === "12-brigadeiros-box") {

      return "AuroraWMDRS-023";
    }
    return productId;
  }

  static get(customerId, body) {
    return `<Order
      AuthorizedClient="Web"
      CarrierServiceCode=""
      CustomerContactID="100000040"
      CustomerEMailID="${customerId}"
      CustomerFirstName="${body.billing_address.first_name}"
      CustomerLastName="${body.billing_address.last_name}"
      CustomerPhoneNo="${body.billing_address.phone}"
      DefaultCustomerInformation="Y"
      DocumentType="0001"
      EnterpriseCode="Aurora"
      EntryType="Call Center"
      FreightTerms=""
      OrderName="${body.order_no}"
      OrderType=""
      PaymentStatus="AUTHORIZED"
      PaymentRuleId="Aurora_DEFAULT"
      SCAC="UPSN"
      ScacAndService=""
      SellerOrganizationCode="Aurora"
      ShipToID="100000120">
    <PriceInfo Currency="${body.currency}"/>
    <OrderLines>
      <OrderLine
        CarrierServiceCode=""
        FulfillmentType=""
        OrderedQty="${body.product_items[0].quantity}"
        PrimeLineNo="1"
        ShipNode="Auro_Store_1"
        PickableFlag="Y"
        DeliveryMethod="PICK"
        SCAC=""
        SubLineNo="1">
        <Item
          ItemID="${this.checkProductId(body.product_items[0].product_id)}"
          UnitCost="${body.product_items[0].base_price}"
          UnitOfMeasure="EACH"/>
        <Notes NumberOfNotes="0"/>
        <KitLines NumberOfKitLines="0"/>
        <Promotions/>
        <LineCharges/>
        <LineTaxes/>
      </OrderLine>
    </OrderLines>
    <OrderLineRelationships/>
    <PaymentMethods>
      <PaymentMethod ChargeSequence="1"
      CreditCardExpDate="12/2020"
      CreditCardName="Thomas White"
      CreditCardNo="4111111111111111"
      CreditCardType="VISA"
      MaxChargeLimit="5000.00"
      PaymentType="CREDIT_CARD"
      UnlimitedCharges="N"/>
    </PaymentMethods>
    <Promotions/>
    <HeaderCharges/>
    <HeaderTaxes/>
    <PersonInfoBillTo
      FirstName="${body.billing_address.first_name}"
      LastName="${body.billing_address.last_name}"
      DayPhone="${body.billing_address.phone}"
      EMailID="twhite@example.com"
      AddressLine1="${body.billing_address.address1}"
      City="${body.billing_address.city}"
      State="${body.billing_address.state_code}"
      ZipCode="${body.billing_address.postal_code}"
      Country="${body.billing_address.country_code}" />
    <PersonInfoShipTo
      FirstName="${body.shipments[0].shipping_address.first_name}"
      LastName="${body.shipments[0].shipping_address.last_name}"
      DayPhone="${body.shipments[0].shipping_address.phone}"
      EMailID="rsmith@yahoo.com"
      AddressLine1="${body.shipments[0].shipping_address.address1}"
      City="${body.shipments[0].shipping_address.city}"
      State="${body.shipments[0].shipping_address.state_code}"
      ZipCode="${body.shipments[0].shipping_address.postal_code}"
      Country="${body.shipments[0].shipping_address.country_code}" />
  </Order>`;
  }
}

module.exports = XMLMock;