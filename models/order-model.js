const XMLmock = require('../config/xml-mock');
const axios = require('axios');

class OrderModel {
  static async createOrderOMS(obj, infoLibrary) {
    const { MessageService, Converters } = obj.privateLibraries;
    try {
      const xmlString = XMLmock.get(obj.req.params.customerId, obj.req.body);
      const URL = `${process.env.OMS_URL}/smcfs/restapi/order`;
      const config = {headers: {'Content-Type': 'text/xml'}};
      const resultXML = await axios.post(URL, xmlString, config);
      return MessageService.getSuccess(resultXML.data, 200);
    } catch (error) {
      return MessageService.getError({}, 500, 'error to try posting order from OMS');
    }
  }

  static async listStatusShipmentOrderOMS(obj, infoLibrary) {
    const { MessageService, Converters } = obj.privateLibraries;
    const { params } = obj.req;
    try {
      const URL = `${process.env.OMS_URL}/smcfs/restapi/order/${params.orderId}`;
      const config = {headers: {'Content-Type': 'text/xml'}};
      const resultXML = await axios.get(URL, config);
      return MessageService.getSuccess(resultXML.data, 200);
    } catch (error) {
      return MessageService.getError({}, 500, 'error to try getting the list of shipment status order from OMS');
    }
  }
}

module.exports = OrderModel;
